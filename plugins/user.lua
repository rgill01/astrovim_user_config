return {
  -- You can also add new plugins here as well:
  -- Add plugins, the lazy syntax
  -- "andweeb/presence.nvim",
  -- {
  --   "ray-x/lsp_signature.nvim",
  --   event = "BufRead",
  --   config = function()
  --     require("lsp_signature").setup()
  --   end,
  -- },
  --
  --
  -- Theme
  {
    "ellisonleao/gruvbox.nvim",
    config = function()
      vim.o.background = "light"
    end,
  },

  -- Language support
  {
    "simrat39/rust-tools.nvim",
  }

}
